# Navbar

## Preview

- [Large screen](/res/adblockplus.org/screenshots/navbar-large.png)
  - [Locale dropdown](/res/adblockplus.org/screenshots/navbar-large-expanded-locales.png)
- [Small screen](/res/adblockplus.org/screenshots/navbar-small.png)
  - [Expanded menu](/res/adblockplus.org/screenshots/navbar-small-expanded-menu.png)
  - [Expanded locales](/res/adblockplus.org/screenshots/navbar-small-expanded-locales.png)

## Functionality

The navbar hides when scrolling down and shows when scrolling up.

The navbar menu collapses into a full-width dropdown on small screens.

The contents of the navbar's locale submenu varies with the translations available for each page. It will not be added to a page until more than 30% of strings in that page are translated into a not default locale.

## Contents

### Brand

| Attribute | Value |
| :-- | :-- |
| 1x src | [Brand 1x](/res/adblockplus.org/static/brand-1x.png) |
| 2x src | [Brand 2x](/res/adblockplus.org/static/brand-2x.svg) |
| text | `Adblock **Plus**` |
| href | `index` |

### Menu Items

```
- [About](about)
- [features](features)
- [Bugs](bugs)
- [Contribute](contribute)
```

### Menu toggle

| Attribute | Value |
| :-- | :-- |
| 1x src | [Toggle 1x](/res/adblockplus.org/static/toggle-1x.png) |
| 2x src | [Toggle 2x](/res/adblockplus.org/static/toggle-2x.svg) |

### Locale item format

```
$LOCALE_NAME ($LOCALE_CODE)
```
